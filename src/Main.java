//import com.zuitt.example.Car;

import com.zuitt.example.*; // * means import all from com.zuitt.example

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");


        //OOP
            //stands for "Object Oriented Programming"
            //OOP is a programming model that allows developer to design software around data or objects, rather than function and logic

        //OOP Concepts
            //Objects - abstract idea that represents something in the real world

            //Class - representation of the object using code
            //Instance - unique copy of the idea, made "Physical"

        //Objects
            //states and attributes - what is the idea about
            //Behaviors - what can idea do
            //Example: A person has attributes like name, age, height and weight. And a person can eat, sleep, and speak

        //Four Pillars of OOP
            //1. Encapsulation
                //data hiding - the variables of a class will be hidden from other classes and can be accessed
                // provide a public and getter function

        //Create a car

        Car myCar = new Car();

        myCar.drive();

        //Assign properties of myCar using the setter methods

        myCar.setName("Toyota");
        myCar.setBrand("Vios");
        myCar.setYearOfMake(2025);

        //to view the properties of myCar using the getter methods
        System.out.println("Car name: " + myCar.getName());
        System.out.println("Car brand: " + myCar.getBrand());
        System.out.println("Year of make: " + myCar.getYearOfMae());

        //Composition and Inheritance
            //Both concepts promote code reuse through different approach
            //"Inheritance" allows modelling an object that is subsets of another objects
            // it defines "is a relationship"
            //to design a class on what it is
            //"Composition" Allows modelling an object that are made up of other objects
                //both entities are dependent on each other
                //composed object cannot exist without the other entity
                //it defines "has a relationship"
                //to design a class on what it does
                //Example:
                    //A Car is a vehicle - inheritance
                    ///A car has a driver - composition

        System.out.println("Driver name: " + myCar.getDriverName());

        myCar.setDriver ("John Smith");
        System.out.println("Driver name: " + myCar.getDriverName());


                //2. Inheritance
                    //can be defined as the process where one class acquires the properties and methods of another class
                    //with the use of inheritance, the information is made manageable in hierarchical order

        Dog myPet = new Dog();

        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.speak();
        myPet.call();

        System.out.println("Pet name: "+ myPet.getName() + ". Breed: " + myPet.getBreed() + ". Color: " + myPet.getColor() );

        //3. Abstraction
        //  is a process where all the logic and complexity are hidden from the user
        // the user would know what to do rather than how it is done

            // Interfaces
                // This is used to achieve total abstraction
                // Creating Abstract classes doesnt support "multiple inheritance". but it can be achieve with interface
                // act as "contacts" wherein a class implements the interface should have the methods that the interface has defined in the class

        Person child = new Person();

        child.sleep();
        child.run();

        child.morningGreet();
        child.holidayGreet();



        // 4. Polymorphism
        // Derived from greek word: poly means "many" and morph means "forms"

        // in short "many forms"

        // Polymorphism is the ability of an object to take on many forms

        //This is usually done by function/method overloading/overriding

        //Two main types of Polymorphism
            //Static or compile time polymorphism
            //Methods with the same name, but they have different data types and aa different number of arguments

        StaticPoly myAddition = new StaticPoly();

        //original method;
        System.out.println(myAddition.addition(5,6));
        //based on adding arguments
        System.out.println(myAddition.addition(5,6,7));
        //based on changing data types
        System.out.println(myAddition.addition(5.5,6.6));

            //2. Dynamic or Run-time polymorphism
            //Function is overridden by replacing the definition of the method in the parent class to the child class
            /*
                Parent Class                    child
                name                            name
                address                         address
                work()                          work()

                work("I am a developer")        work("I am a manager")


            */

        Child myChild = new Child();
        myChild.speak();








    }



}